import {ListClasses} from '../classes/list';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

export const LISTS: Array<ListClasses> = [
  {
    user_id: 'id_1',
    user_status: 'Active',
    user_fname: 'Jane',
    user_lname: 'Smith',
    user_birthday: 'Sun Dec 05 1993 00:00:00 GMT+0200',
    user_email: 'mail@mail.com'
  },
  {
    user_id: 'id_2',
    user_status: 'Active',
    user_fname: 'Jane',
    user_lname: 'Smith',
    user_birthday: 'Sun Dec 05 1993 00:00:00 GMT+0200',
    user_email: 'mail@mail.com'
  },
  {
    user_id: 'id_3',
    user_status: 'Active',
    user_fname: 'Julia',
    user_lname: 'Anderson',
    user_birthday: 'Sun Dec 05 1993 00:00:00 GMT+0200',
    user_email: 'mail@mail.com'
  },
  {
    user_id: 'id_4',
    user_status: 'Active',
    user_fname: 'Anna',
    user_lname: 'Filipp',
    user_birthday: 'Sun Dec 05 1993 00:00:00 GMT+0200',
    user_email: 'mail@mail.com'
  },
  {
    user_id: 'id_5',
    user_status: 'Active',
    user_fname: 'Jane',
    user_lname: 'Smith',
    user_birthday: 'Sun Dec 05 1993 00:00:00 GMT+0200',
    user_email: 'mail@mail.com'
  }
];
