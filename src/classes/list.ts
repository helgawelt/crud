import {Injectable} from '@angular/core';

@Injectable()
export class ListClasses {
  user_id: string;
  user_status: string;
  user_fname: string;
  user_lname: string;
  user_birthday: string;
  user_email: string;
}
