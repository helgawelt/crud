import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {_throw} from 'rxjs/observable/throw';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

@Injectable()
export class ParamInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return _throw('This is test project');
  }
}
