import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

export interface Posts {
  user_id: string;
  user_status: string;
  user_fname: string;
  user_lname: string;
  user_birthday: string;
  user_email: string;
}

@Injectable()
export class ApiService {

  private postsURL = 'http://localhost';

  constructor(private http: HttpClient) { }

  getData(): Observable<Posts> {
    return this.http
      .get<Posts>(this.postsURL);
  }

}
