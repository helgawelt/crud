import { Injectable } from '@angular/core';
import { LISTS } from '../../Data/user-list';
import { ListClasses } from '../../classes/list';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class ListService {

  constructor() { }

  getList(id: string): Observable<ListClasses> {
    return of(LISTS.find(item => item.user_id === id));
  }
}
