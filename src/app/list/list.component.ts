import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import { LISTS } from '../../Data/user-list';
import { Sort } from '@angular/material';
import {ListService} from "../../services/list/list.service";
import {HttpClient} from '@angular/common/http';
import {ListClasses} from "../../classes/list";
import {ApiService} from "../../services/api/api.service";
import { DatePipe } from '@angular/common';
import { ParamInterceptor } from '../../classes/api.interceptor';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  list = LISTS;
  item: ListClasses = {
    user_id:'',
    user_status: '',
    user_fname: '',
    user_lname: '',
    user_birthday: '',
    user_email: '',
  };
  newUser: ListClasses = {
    user_id:'id_next',
    user_status: 'Active',
    user_fname: '',
    user_lname: '',
    user_birthday: '',
    user_email: '',
  };
  sortedData;
  viewShow = false;
  editShow = false;
  addShow = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private listService: ListService,
              private http: HttpClient,
              private api: ApiService
  ) {
    this.sortedData = this.list.slice();
  }

  ngOnInit() {

  }

  getList(user, type): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.listService.getList(user.user_id)
      .subscribe(item => this.item = item);

    this.item.user_birthday = new DatePipe('en-US').transform(this.item.user_birthday, 'dd/MM/yyyy');

    if (type === 'view') {
      this.viewShow = true;
      this.editShow = false;
    }
    else if (type === 'edit') {
      this.editShow = true;
      this.viewShow = false;
    }
  }

  closePopup() {
    this.viewShow = false;
    this.editShow = false;
    this.addShow = false;
  }

  openAddPopup() {
    this.addShow = true;

    this.newUser.user_birthday = new DatePipe('en-US').transform(this.newUser.user_birthday, 'dd/MM/yyyy');
  }

  addUser() {
    this.sortedData.push(this.newUser);
    this.list.push(this.newUser);

    this.newUser = {
      user_id:'id_next',
      user_status: 'Active',
      user_fname: '',
      user_lname: '',
      user_birthday: '',
      user_email: '',
    };

    this.closePopup();

    return this.http
      .post('localhost' + '/add', {
        data: {
          user_id: this.item.user_id,
          user_status: 'Active',
          user_fname: this.item.user_fname,
          user_lname: this.item.user_lname,
          user_birthday: this.item.user_birthday,
          user_email: this.item.user_email
        }
      })
      .subscribe(
        (response: any) => {
          console.log(response);
        },
        (err: any) => {
          console.log(err);
        }
      )
  }

  saveChanges() {
    if (this.sortedData.indexOf(this.item) !== -1) {
      this.sortedData[this.sortedData.indexOf(this.item)] = this.item;
    }

    if (this.list.indexOf(this.item) !== -1) {
      this.list[this.list.indexOf(this.item)] = this.item;
    }

    this.closePopup();

    return this.http
      .post('localhost' + '/edit', {
        data: {
          user_id: this.item.user_id,
          user_status: 'Active',
          user_fname: this.item.user_fname,
          user_lname: this.item.user_lname,
          user_birthday: this.item.user_birthday,
          user_email: this.item.user_email
        }
      })
      .subscribe(
        (response: any) => {
          console.log(response);
        },
        (err: any) => {
          console.log(err);
        }
      )
  }

  delete(item) {
    if (this.sortedData.indexOf(item) !== -1) {
      this.sortedData.splice(this.list.indexOf(item), 1);
    }

    return this.http
      .post('localhost' + '/delete_user', {
        params:  item.user_id
      })
      .subscribe(
        (response: any) => {
          console.log(response);
        },
        (err: any) => {
          console.log(err);
        }
      )
  }

  sortData(sort: Sort) {
    const data = this.list.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      let isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'user_id': return compare(+a.user_id, +b.user_id, isAsc);
        case 'user_fname': return compare(+a.user_fname, +b.user_fname, isAsc);
        case 'user_lname': return compare(+a.user_lname, +b.user_lname, isAsc);
        default: return 0;
      }
    });
  }
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

