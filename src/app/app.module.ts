import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatInputModule, MatTableModule, MatToolbarModule, MatNativeDateModule  } from '@angular/material';
import {MatDatepickerModule, MatFormFieldModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSortModule} from '@angular/material';
import {MatCommonModule} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {NgxMaskModule} from 'ngx-mask';
import { BsDatepickerModule, BsDropdownModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ListComponent } from './list/list.component';
import {ListService} from "../services/list/list.service";
import {ApiService} from "../services/api/api.service";
import { ParamInterceptor } from '../classes/api.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    MatToolbarModule,
    MatInputModule,
    MatTableModule,
    AppRoutingModule,
    MatSortModule,
    MatCommonModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    HttpClientModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    NgxMaskModule.forRoot(),
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
  ],
  exports: [
    BsDatepickerModule
  ],
  providers: [ListService,
    MatDatepickerModule,
    ApiService, {
        provide: HTTP_INTERCEPTORS,
        useClass: ParamInterceptor,
        multi: true
      }],
  bootstrap: [AppComponent]
})
export class AppModule { }
