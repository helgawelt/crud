import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {ListComponent} from "./list/list.component";

const routes: Routes = [
  { path: '',  redirectTo: '/list', pathMatch: 'full' },
  { path: 'list', component: ListComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
